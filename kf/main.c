#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "parse.h"
#include "kalman.h"
#include "sensors.h"

void
help (char *exec)
{
  printf ("Usage:\v%s [options]\n", exec);
  printf ("\nOptions:\n");
  printf ("  -h                 Print this message.\n");
  printf ("  -i imu_file        IMU Input file for process.\n");
  printf ("  -p params_file     KF Parameters file.\n");
}

void
show_quaternion (float timestamp, kalman_filter_t *kf)
{
  int j;

  printf ("%f  ", timestamp);
  for ( j = 0; j < 4; j++)
    printf ("%f  ", kalman_filter_matrix_get_element (kf->x, j, 0));
  printf ("\n");
}

int
main (int argc, char **argv)
{
  int i;

  int c;
  int index;
  int non_option_argument = 0;

  char *imu_input_file;
  char *params_input_file;

  sensors_t imu_log = {0};
  kalman_filter_t kf = {0};
  kalman_filter_params_t kf_params = {0};
  float dt;

  opterr = 0;

  while ((c = getopt (argc, argv, "hi:p:")) != -1)
    switch (c)
      {
      case 'h':
        help(argv[0]);
        break;
      case 'i':
        imu_input_file = optarg;
        break;
      case 'p':
        params_input_file = optarg;
        break;
      case '?':
        if (optopt == 'i')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (optopt == 'p')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        exit(EXIT_FAILURE);
      }

  for (index = optind; index < argc; index++, non_option_argument = 1)
    printf ("Non-option argument %s\n", argv[index]);

  if (non_option_argument || argc == 1)
  {
    printf("non option\n");
    help(argv[0]);
    return 1;
  }

  /*dismiss parse_XXX in embedded applications*/
  /*load all sensor measurements from imu_input_file to imu_log*/
  parse_sensors_file (imu_input_file, &imu_log);
  /*load kalman filter parameters from params_input_file to kf_params*/
  parse_params_file (params_input_file, &kf_params);

  /*Prepare kalman filter data structures*/
  kalman_filter_alloc (&kf);
  kalman_filter_init (&kf, &kf_params);

  /*Kalman Filter Loop*/
  for (i = 0; i < imu_log.n-1; i++)
  {
    dt = imu_log.timestamp[i+1] - imu_log.timestamp[i];

    /*when gyro measurement is available do*/
    kalman_filter_predict_stage (&kf, &imu_log.gyro[i], dt);

    /*when accel measurement is available do*/
    kalman_filter_accel_update_stage (&kf, &imu_log.accel[i]) ;

    /*when magneto measurement is available do*/
    kalman_filter_mag_update_stage (&kf, &imu_log.magneto[i]) ;

    /* kf.x has the estimated quaternion */
    show_quaternion(imu_log.timestamp[i], &kf);
  }
  /* repite last estimation for plotting purposes */
  show_quaternion(imu_log.timestamp[i], &kf);

  /* free all structures */
  sensors_array_free (&imu_log);
  kalman_filter_free (&kf);

  return 0;
}

