#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "kalman.h"
#include "sensors.h"

matrix_t
kalman_filter_matrix_alloc (int row, int col)
{
  int i;
  matrix_t m;

  m.row = row;
  m.col = col;

  m.mat = malloc( row * sizeof (double *) );
  m.mat[0] = malloc ( row*col * sizeof (double) );

  for ( i = 1 ; i < row ; i++ )
    m.mat[i] = m.mat[i-1] + col;

  return m;
}

void
kalman_filter_alloc (kalman_filter_t *kf)
{
  kf->x = kalman_filter_matrix_alloc(4, 1);
  kf->x_ = kalman_filter_matrix_alloc(4, 1);
  kf->P = kalman_filter_matrix_alloc(4, 4);
  kf->P_ = kalman_filter_matrix_alloc(4, 4);
  kf->F = kalman_filter_matrix_alloc(4, 4);
  kf->FT = kalman_filter_matrix_alloc(4, 4);
  kf->Ha = kalman_filter_matrix_alloc(3, 4);
  kf->HaT = kalman_filter_matrix_alloc(4, 3);
  kf->Hm = kalman_filter_matrix_alloc(3, 4);
  kf->HmT = kalman_filter_matrix_alloc(4, 3);
  kf->Q = kalman_filter_matrix_alloc(4, 4);
  kf->Ra = kalman_filter_matrix_alloc(3, 3);
  kf->Rm = kalman_filter_matrix_alloc(3, 3);
  kf->FP = kalman_filter_matrix_alloc(4, 4);
  kf->z = kalman_filter_matrix_alloc(3, 1);
  kf->z_hat = kalman_filter_matrix_alloc(3, 1);
  kf->y = kalman_filter_matrix_alloc(3, 1);
  kf->xe = kalman_filter_matrix_alloc(4, 1);
  kf->S = kalman_filter_matrix_alloc(3, 3);
  kf->S_inv = kalman_filter_matrix_alloc(3, 3);
  kf->PHT = kalman_filter_matrix_alloc(4, 3);
  kf->KH = kalman_filter_matrix_alloc(4, 4);
  kf->KHP = kalman_filter_matrix_alloc(4, 4);
  kf->PNJ = kalman_filter_matrix_alloc(4, 4);
  kf->K = kalman_filter_matrix_alloc(4, 3);
  kf->I = kalman_filter_matrix_alloc(4, 4);
  kf->norm_jacobian = kalman_filter_matrix_alloc(4, 4);
  kf->norm_jacobianT = kalman_filter_matrix_alloc(4, 4);
  kf->Rb2n = kalman_filter_matrix_alloc(3, 3);
  kf->Rn2b = kalman_filter_matrix_alloc(3, 3);
  kf->z_mr = kalman_filter_matrix_alloc(3, 1);
}

void
kalman_filter_matrix_set_zero (matrix_t m)
{
  int i, j;

  for (i = 0; i < m.row; i++)
    for (j = 0; j < m.col; j++)
      m.mat[i][j] = 0;

}

void
kalman_filter_matrix_set_element (matrix_t m, int i, int j, double val)
{
  m.mat[i][j] = val;
}

double
kalman_filter_matrix_get_element (matrix_t m, int i, int j)
{
  return m.mat[i][j];
}

void
kalman_filter_matrix_scale (matrix_t m, double val)
{
  int i, j;

  for (i = 0; i < m.row; i++)
    for (j = 0; j < m.col; j++)
      m.mat[i][j] *= val ;

}

void
kalman_filter_matrix_add (matrix_t m, matrix_t n)
{
  int i, j;

  for (i = 0; i < m.row; i++)
    for (j = 0; j < m.col; j++)
      m.mat[i][j] += n.mat[i][j];

}

void
kalman_filter_matrix_sub (matrix_t m, matrix_t n)
{
  int i, j;

  for (i = 0; i < m.row; i++)
    for (j = 0; j < m.col; j++)
      m.mat[i][j] -= n.mat[i][j];

}

void
kalman_filter_matrix_set_identity (matrix_t m, int side)
{
  int i;

  kalman_filter_matrix_set_zero (m);
  for (i = 0; i < side; i++)
    kalman_filter_matrix_set_element (m, i, i, 1);

}

void
kalman_filter_matrix_mult (matrix_t a, matrix_t b, matrix_t c)
{
  int i,j,k;

  for ( i = 0 ; i < a.row ; i++)
    for ( j = 0 ; j < b.col ; j++ )
      for ( k = 0 ; k < a.col ; k++ )
        c.mat[i][j] += a.mat[i][k] * b.mat[k][j];

}

void
kalman_filter_matrix_transpose (matrix_t a, matrix_t at)
{
  int i,j;

  for ( i = 0 ; i < a.row ; i++)
    for ( j = 0 ; j < a.col ; j++ )
        at.mat[j][i] = a.mat[i][j];

}

void
kalman_filter_matrix_memcpy (matrix_t a, matrix_t b)
{
  int i,j;

  for ( i = 0 ; i < a.row ; i++)
    for ( j = 0 ; j < a.col ; j++ )
      b.mat[i][j] = a.mat[i][j];

}

double
kalman_filter_matrix_det (matrix_t m)
{
  double det;

  det =  m.mat[0][0] * (m.mat[1][1]*m.mat[2][2] - m.mat[1][2] * m.mat[2][1]);
  det -= m.mat[0][1] * (m.mat[1][0]*m.mat[2][2] - m.mat[1][2] * m.mat[2][0]);
  det += m.mat[0][2] * (m.mat[1][0]*m.mat[2][1] - m.mat[1][1] * m.mat[2][0]);

  return det;
}

void
kalman_filter_matrix_scale_adjoint (matrix_t m, matrix_t a, double s)
{
  a.mat[0][0] = s * (m.mat[1][1] * m.mat[2][2] - m.mat[1][2] * m.mat[2][1]);
  a.mat[1][0] = s * (m.mat[1][2] * m.mat[2][0] - m.mat[1][0] * m.mat[2][2]);
  a.mat[2][0] = s * (m.mat[1][0] * m.mat[2][1] - m.mat[1][1] * m.mat[2][0]);

  a.mat[0][1] = s * (m.mat[0][2] * m.mat[2][1] - m.mat[0][1] * m.mat[2][2]);
  a.mat[1][1] = s * (m.mat[0][0] * m.mat[2][2] - m.mat[0][2] * m.mat[2][0]);
  a.mat[2][1] = s * (m.mat[0][1] * m.mat[2][0] - m.mat[0][0] * m.mat[2][1]);

  a.mat[0][2] = s * (m.mat[0][1] * m.mat[1][2] - m.mat[0][2] * m.mat[1][1]);
  a.mat[1][2] = s * (m.mat[0][2] * m.mat[1][0] - m.mat[0][0] * m.mat[1][2]);
  a.mat[2][2] = s * (m.mat[0][0] * m.mat[1][1] - m.mat[0][1] * m.mat[1][0]);
}

void
kalman_filter_matrix_invert (matrix_t m, matrix_t mi)
{
  double invdet;

  invdet = 1/kalman_filter_matrix_det (m);
  kalman_filter_matrix_scale_adjoint (m, mi, invdet);
}

void
kalman_filter_init (kalman_filter_t *kf, kalman_filter_params_t *kf_params)
{
  kalman_filter_matrix_set_zero (kf->x);
  kalman_filter_matrix_set_element (kf->x, 0, 0, 1); // Unit quaternion to start
  kalman_filter_matrix_set_zero (kf->x_);

  kalman_filter_matrix_set_zero (kf->P);
  kalman_filter_matrix_set_element (kf->P, 0, 0, kf_params->p11);
  kalman_filter_matrix_set_element (kf->P, 1, 1, kf_params->p22);
  kalman_filter_matrix_set_element (kf->P, 2, 2, kf_params->p33);
  kalman_filter_matrix_set_element (kf->P, 3, 3, kf_params->p44);
  kalman_filter_matrix_set_zero (kf->P_);

  kalman_filter_matrix_set_zero (kf->Q);
  kalman_filter_matrix_set_element (kf->Q, 0, 0, kf_params->q11);
  kalman_filter_matrix_set_element (kf->Q, 1, 1, kf_params->q22);
  kalman_filter_matrix_set_element (kf->Q, 2, 2, kf_params->q33);
  kalman_filter_matrix_set_element (kf->Q, 3, 3, kf_params->q44);

  kalman_filter_matrix_set_zero (kf->Ra);
  kalman_filter_matrix_set_element (kf->Ra, 0, 0, kf_params->raxx);
  kalman_filter_matrix_set_element (kf->Ra, 1, 1, kf_params->rayy);
  kalman_filter_matrix_set_element (kf->Ra, 2, 2, kf_params->rayy);

  kalman_filter_matrix_set_zero (kf->Rm);
  kalman_filter_matrix_set_element (kf->Rm, 0, 0, kf_params->rmxx);
  kalman_filter_matrix_set_element (kf->Rm, 1, 1, kf_params->rmyy);
  kalman_filter_matrix_set_element (kf->Rm, 2, 2, kf_params->rmyy);

  kalman_filter_matrix_set_zero (kf->F);
  kalman_filter_matrix_set_zero (kf->FT);
  kalman_filter_matrix_set_zero (kf->Ha);
  kalman_filter_matrix_set_zero (kf->HaT);
  kalman_filter_matrix_set_zero (kf->Hm);
  kalman_filter_matrix_set_zero (kf->HmT);
  kalman_filter_matrix_set_zero (kf->FP);

  kalman_filter_matrix_set_zero (kf->z);
  kalman_filter_matrix_set_zero (kf->z_hat);
  kalman_filter_matrix_set_zero (kf->y);

  kalman_filter_matrix_set_zero (kf->S);
  kalman_filter_matrix_set_zero (kf->S_inv);
  kalman_filter_matrix_set_zero (kf->PHT);
  kalman_filter_matrix_set_zero (kf->KH);
  kalman_filter_matrix_set_zero (kf->KHP);
  kalman_filter_matrix_set_zero (kf->PNJ);
  kalman_filter_matrix_set_identity (kf->I, 4);

  kalman_filter_matrix_set_zero (kf->K);
  kalman_filter_matrix_set_zero (kf->xe);

  kalman_filter_matrix_set_zero (kf->norm_jacobian);
  kalman_filter_matrix_set_zero (kf->norm_jacobianT);
  kalman_filter_matrix_set_zero (kf->Rb2n);
  kalman_filter_matrix_set_zero (kf->Rn2b);
  kalman_filter_matrix_set_zero (kf->z_mr);
}

void
kalman_filter_set_Fk (kalman_filter_t *kf, sensor3D_t *gyro, float dt)
{
  /*
  F_k = \frac{1}{2} \Omega(\omega) \Delta t + I
  where
   \Omega(\vect{\omega}(t)) =
    \begin{bmatrix}
      0 & -\omega_x & -\omega_y & -\omega_z\\
      \omega_x & 0 & \omega_z & -\omega_y\\
      \omega_y & -\omega_z & 0 & \omega_x\\
      \omega_z & \omega_y & -\omega_x & 0
    \end{bmatrix}
  */

  kalman_filter_matrix_set_element (kf->F, 0, 0, 0);
  kalman_filter_matrix_set_element (kf->F, 0, 1, -gyro->x);
  kalman_filter_matrix_set_element (kf->F, 0, 2, -gyro->y);
  kalman_filter_matrix_set_element (kf->F, 0, 3, -gyro->z);

  kalman_filter_matrix_set_element (kf->F, 1, 0, gyro->x);
  kalman_filter_matrix_set_element (kf->F, 1, 1, 0);
  kalman_filter_matrix_set_element (kf->F, 1, 2, gyro->z);
  kalman_filter_matrix_set_element (kf->F, 1, 3, -gyro->y);

  kalman_filter_matrix_set_element (kf->F, 2, 0, gyro->y);
  kalman_filter_matrix_set_element (kf->F, 2, 1, -gyro->z);
  kalman_filter_matrix_set_element (kf->F, 2, 2, 0);
  kalman_filter_matrix_set_element (kf->F, 2, 3, gyro->x);

  kalman_filter_matrix_set_element (kf->F, 3, 0, gyro->z);
  kalman_filter_matrix_set_element (kf->F, 3, 1, gyro->y);
  kalman_filter_matrix_set_element (kf->F, 3, 2, -gyro->x);
  kalman_filter_matrix_set_element (kf->F, 3, 3, 0);

  kalman_filter_matrix_scale (kf->F, .5*dt);

  kalman_filter_matrix_add (kf->F, kf->I);

}

void
kalman_filter_set_Hak (kalman_filter_t * kf, double *q)
{
  /*
  H_{ak} =
  \begin{bmatrix}
    -2q_2 &  2q_3 & -2q_0 & 2q_1 \\
    2q_1 &  2q_0 &  2q_3 & 2q_2 \\
    2q_0 & -2q_1 & -2q_2 & 2q_3
  \end{bmatrix}
  */

  kalman_filter_matrix_set_element (kf->Ha, 0, 0, -2*q[2]);
  kalman_filter_matrix_set_element (kf->Ha, 0, 1, 2*q[3]);
  kalman_filter_matrix_set_element (kf->Ha, 0, 2, -2*q[0]);
  kalman_filter_matrix_set_element (kf->Ha, 0, 3, 2*q[1]);

  kalman_filter_matrix_set_element (kf->Ha, 1, 0, 2*q[1]);
  kalman_filter_matrix_set_element (kf->Ha, 1, 1, 2*q[0]);
  kalman_filter_matrix_set_element (kf->Ha, 1, 2, 2*q[3]);
  kalman_filter_matrix_set_element (kf->Ha, 1, 3, 2*q[2]);

  kalman_filter_matrix_set_element (kf->Ha, 2, 0, 2*q[0]);
  kalman_filter_matrix_set_element (kf->Ha, 2, 1, -2*q[1]);
  kalman_filter_matrix_set_element (kf->Ha, 2, 2, -2*q[2]);
  kalman_filter_matrix_set_element (kf->Ha, 2, 3, 2*q[3]);
}

void
kalman_filter_set_z_hat_ak (kalman_filter_t * kf)
{
  double q[4];
  /*
  \hat{z}_{ak} = h_a(\hat{x}^-_k) = g % g is usually normalized
  \begin{bmatrix}
    2(q_1q_3-q_2q_0)\\
    2(q_2q_3+q_1q_0)\\
    q_0^2-q_1^2-q_2^2+q_3^2
  \end{bmatrix}
  */

  q[0] = kalman_filter_matrix_get_element (kf->x_, 0, 0);
  q[1] = kalman_filter_matrix_get_element (kf->x_, 1, 0);
  q[2] = kalman_filter_matrix_get_element (kf->x_, 2, 0);
  q[3] = kalman_filter_matrix_get_element (kf->x_, 3, 0);

  kalman_filter_set_Hak (kf, q);

  kalman_filter_matrix_set_element (kf->z_hat, 0, 0, 2*(q[1]*q[3]-q[2]*q[0]));
  kalman_filter_matrix_set_element (kf->z_hat, 1, 0, 2*(q[2]*q[3]+q[1]*q[0]));
  kalman_filter_matrix_set_element (kf->z_hat, 2, 0, q[0]*q[0]-q[1]*q[1]-q[2]*q[2]+q[3]*q[3]);

}

void
kalman_filter_set_Hmk (kalman_filter_t * kf, double *q)
{
  /*
  H_{mk} =
  \begin{bmatrix}
    2q_0 & 2q_1 & -2q_2 & -2q_3 \\
    -2q_3 & 2q_2 &  2q_1 & -2q_0 \\
    2q_2 & 2q_3 &  2q_0 &  2q_1
  \end{bmatrix}
  */

  kalman_filter_matrix_set_element (kf->Hm, 0, 0, 2*q[0]);
  kalman_filter_matrix_set_element (kf->Hm, 0, 1, 2*q[1]);
  kalman_filter_matrix_set_element (kf->Hm, 0, 2, -2*q[2]);
  kalman_filter_matrix_set_element (kf->Hm, 0, 3, -2*q[3]);

  kalman_filter_matrix_set_element (kf->Hm, 1, 0, -2*q[3]);
  kalman_filter_matrix_set_element (kf->Hm, 1, 1, 2*q[2]);
  kalman_filter_matrix_set_element (kf->Hm, 1, 2, 2*q[1]);
  kalman_filter_matrix_set_element (kf->Hm, 1, 3, -2*q[0]);

  kalman_filter_matrix_set_element (kf->Hm, 2, 0, 2*q[2]);
  kalman_filter_matrix_set_element (kf->Hm, 2, 1, 2*q[3]);
  kalman_filter_matrix_set_element (kf->Hm, 2, 2, 2*q[0]);
  kalman_filter_matrix_set_element (kf->Hm, 2, 3, 2*q[1]);
}

void
kalman_filter_set_z_hat_mk (kalman_filter_t * kf)
{
  double q[4];
  /*
  \hat{z}_{mk} = h_m(\hat{x}^-_k) =
  \begin{bmatrix}
    q_0^2+q_1^2-q_2^2-q_3^2\\
    2(q_1q_2-q_3q_0)\\
    2(q_1q_3+q_2q_0)
  \end{bmatrix}
  */

  q[0] = kalman_filter_matrix_get_element (kf->x_, 0, 0);
  q[1] = kalman_filter_matrix_get_element (kf->x_, 1, 0);
  q[2] = kalman_filter_matrix_get_element (kf->x_, 2, 0);
  q[3] = kalman_filter_matrix_get_element (kf->x_, 3, 0);

  kalman_filter_set_Hmk (kf, q);

  kalman_filter_matrix_set_element (kf->z_hat, 0, 0, q[0]*q[0]+q[1]*q[1]-q[2]*q[2]-q[3]*q[3]);
  kalman_filter_matrix_set_element (kf->z_hat, 1, 0, 2*(q[1]*q[2]-q[3]*q[0]));
  kalman_filter_matrix_set_element (kf->z_hat, 2, 0, 2*(q[1]*q[3]+q[2]*q[0]));

}

void
kalman_filter_predict_stage (kalman_filter_t * kf, sensor3D_t * gyro, float dt)
{
  kalman_filter_set_Fk (kf, gyro, dt);

  // x_{k}^{-} = F_{k} * x_{k-1}
  kalman_filter_matrix_set_zero (kf->x_);
  kalman_filter_matrix_mult (kf->F, kf->x, kf->x_);

  // P_{k}^{-} = F_{k} * P_{k-1} * F_{k}^{T} + Q_{k-1}
  kalman_filter_matrix_set_zero (kf->FP);
  kalman_filter_matrix_mult (kf->F, kf->P, kf->FP);
  kalman_filter_matrix_transpose (kf->F, kf->FT);
  kalman_filter_matrix_set_zero (kf->P_);
  kalman_filter_matrix_mult (kf->FP, kf->FT, kf->P_);
  kalman_filter_matrix_add (kf->P_, kf->Q);
}

void
kalman_filter_accel_update_stage (kalman_filter_t * kf, sensor3D_t * accel )
{
  kalman_filter_set_z_hat_ak (kf);

  kalman_filter_matrix_set_element (kf->z, 0, 0, accel->x);
  kalman_filter_matrix_set_element (kf->z, 1, 0, accel->y);
  kalman_filter_matrix_set_element (kf->z, 2, 0, accel->z);

  // S = H_{k} * P_{k}^{-} * H_{k}^{T} + R_{k}
  kalman_filter_matrix_set_zero (kf->PHT);
  kalman_filter_matrix_transpose (kf->Ha, kf->HaT);
  kalman_filter_matrix_mult (kf->P_, kf->HaT, kf->PHT);
  kalman_filter_matrix_set_zero (kf->S);
  kalman_filter_matrix_mult (kf->Ha, kf->PHT, kf->S);
  kalman_filter_matrix_add (kf->S, kf->Ra);

  // K_{k} =  P_{k}^{-} * H_{k}^{T} * S^{-1}
  kalman_filter_matrix_invert (kf->S, kf->S_inv);
  kalman_filter_matrix_set_zero (kf->K);
  kalman_filter_matrix_mult (kf->PHT, kf->S_inv, kf->K);

  // x_{k} = x_{k-1} + K_{k} * (z - \hat{z})
  kalman_filter_matrix_memcpy (kf->z, kf->y);
  kalman_filter_matrix_sub (kf->y, kf->z_hat);
  kalman_filter_matrix_set_zero (kf->xe);
  kalman_filter_matrix_mult (kf->K, kf->y, kf->xe);
  kalman_filter_matrix_memcpy (kf->x_, kf->x);
  kalman_filter_matrix_add (kf->x, kf->xe);

  // P_{k} = P_{k}^{-} - K_{k} * H_{k} * P_{k}^{-}
  kalman_filter_matrix_set_zero (kf->KH);
  kalman_filter_matrix_mult (kf->K, kf->Ha, kf->KH);
  kalman_filter_matrix_set_zero (kf->KHP);
  kalman_filter_matrix_mult (kf->KH, kf->P_, kf->KHP);
  kalman_filter_matrix_memcpy (kf->P_, kf->P);
  kalman_filter_matrix_sub (kf->P, kf->KHP);

  kalman_filter_quaternion_normalization (kf);

  kalman_filter_matrix_memcpy (kf->x, kf->x_);
  kalman_filter_matrix_memcpy (kf->P, kf->P_);
}

void
kalman_filter_mag_update_stage (kalman_filter_t * kf, sensor3D_t * mag )
{
  kalman_filter_set_z_hat_mk (kf);

  kalman_filter_matrix_set_element (kf->z, 0, 0, mag->x);
  kalman_filter_matrix_set_element (kf->z, 1, 0, mag->y);
  kalman_filter_matrix_set_element (kf->z, 2, 0, mag->z);

  kalman_filter_magneto_measurements_normalization (kf);

  // S = H_{k} * P_{k}^{-} * H_{k}^{T} + R_{k}
  kalman_filter_matrix_set_zero (kf->PHT);
  kalman_filter_matrix_transpose (kf->Hm, kf->HmT);
  kalman_filter_matrix_mult (kf->P_, kf->HmT, kf->PHT);
  kalman_filter_matrix_set_zero (kf->S);
  kalman_filter_matrix_mult (kf->Hm, kf->PHT, kf->S);
  kalman_filter_matrix_add (kf->S, kf->Rm);

  // K_{k} =  P_{k}^{-} * H_{k}^{T} * S^{-1}
  kalman_filter_matrix_invert (kf->S, kf->S_inv);
  kalman_filter_matrix_set_zero (kf->K);
  kalman_filter_matrix_mult (kf->PHT, kf->S_inv, kf->K);

  // x_{k} = x_{k-1} + K_{k} * (z - \hat{z})
  kalman_filter_matrix_memcpy (kf->z, kf->y);
  kalman_filter_matrix_sub (kf->y, kf->z_hat);
  kalman_filter_matrix_set_zero (kf->xe);
  kalman_filter_matrix_mult (kf->K, kf->y, kf->xe);
  kalman_filter_matrix_memcpy (kf->x_, kf->x);
  kalman_filter_matrix_add (kf->x, kf->xe);

  // P_{k} = P_{k}^{-} - K_{k} * H_{k} * P_{k}^{-}
  kalman_filter_matrix_set_zero (kf->KH);
  kalman_filter_matrix_mult (kf->K, kf->Hm, kf->KH);
  kalman_filter_matrix_set_zero (kf->KHP);
  kalman_filter_matrix_mult (kf->KH, kf->P_, kf->KHP);
  kalman_filter_matrix_memcpy (kf->P_, kf->P);
  kalman_filter_matrix_sub (kf->P, kf->KHP);

  kalman_filter_quaternion_normalization (kf);
}

void
kalman_filter_quaternion_normalization (kalman_filter_t * kf)
{
  double norm;
  double q[4];
  double scale;

  norm = kf->x.mat[0][0]*kf->x.mat[0][0];
  norm +=kf->x.mat[1][0]*kf->x.mat[1][0];
  norm +=kf->x.mat[2][0]*kf->x.mat[2][0];
  norm +=kf->x.mat[3][0]*kf->x.mat[3][0];
  norm = sqrt(norm);

  kalman_filter_matrix_scale (kf->x, 1/norm);

  q[0] = kalman_filter_matrix_get_element (kf->x, 0, 0);
  q[1] = kalman_filter_matrix_get_element (kf->x, 1, 0);
  q[2] = kalman_filter_matrix_get_element (kf->x, 2, 0);
  q[3] = kalman_filter_matrix_get_element (kf->x, 3, 0);

  kalman_filter_matrix_set_element (kf->norm_jacobian, 0, 0, q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 0, 1, -q[0]*q[1]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 0, 2, -q[0]*q[2]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 0, 3, -q[0]*q[3]);

  kalman_filter_matrix_set_element (kf->norm_jacobian, 1, 0, -q[0]*q[1]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 1, 1, q[0]*q[0] + q[2]*q[2] + q[3]*q[3]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 1, 2, -q[2]*q[1]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 1, 3, -q[3]*q[1]);

  kalman_filter_matrix_set_element (kf->norm_jacobian, 2, 0, -q[0]*q[2]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 2, 1, -q[1]*q[2]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 2, 2, q[0]*q[0] + q[1]*q[1] + q[3]*q[3]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 2, 3, -q[3]*q[2]);

  kalman_filter_matrix_set_element (kf->norm_jacobian, 3, 0, -q[0]*q[3]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 3, 1, -q[1]*q[3]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 3, 2, -q[2]*q[3]);
  kalman_filter_matrix_set_element (kf->norm_jacobian, 3, 3, q[0]*q[0] + q[1]*q[1] + q[2]*q[2]);

  scale = pow (q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3],-1.5);
  kalman_filter_matrix_scale (kf->norm_jacobian, scale);

  kalman_filter_matrix_set_zero (kf->PNJ);
  kalman_filter_matrix_transpose (kf->norm_jacobian, kf->norm_jacobianT);
  kalman_filter_matrix_mult (kf->P, kf->norm_jacobianT, kf->PNJ);
  kalman_filter_matrix_set_zero (kf->P);
  kalman_filter_matrix_mult (kf->norm_jacobian, kf->PNJ, kf->P);

}

void
kalman_filter_change_rep_to_navigation (kalman_filter_t * kf)
{
  double q[4];

  q[0] = kalman_filter_matrix_get_element (kf->x_, 0, 0);
  q[1] = kalman_filter_matrix_get_element (kf->x_, 1, 0);
  q[2] = kalman_filter_matrix_get_element (kf->x_, 2, 0);
  q[3] = kalman_filter_matrix_get_element (kf->x_, 3, 0);

  /*
    R = np.array([
        [-q3**2-q2**2+q1**2+q0**2,2*(q1*q2-q0*q3),2*(q1*q3+q0*q2)],
        [2*(q0*q3+q1*q2),-q3**2+q2**2-q1**2+q0**2,2*(q2*q3-q0*q1)],
        [2*(q1*q3-q0*q2),2*(q2*q3+q0*q1),q3**2-q2**2-q1**2+q0**2]
        ])
  */

  kalman_filter_matrix_set_element (kf->Rb2n, 0, 0, -q[3]*q[3]-q[2]*q[2]+q[1]*q[1]+q[0]*q[0]);
  kalman_filter_matrix_set_element (kf->Rb2n, 0, 1, 2*(q[1]*q[2]-q[0]*q[3]));
  kalman_filter_matrix_set_element (kf->Rb2n, 0, 2, 2*(q[1]*q[3]+q[0]*q[2]));

  kalman_filter_matrix_set_element (kf->Rb2n, 1, 0, 2*(q[0]*q[3]+q[1]*q[2]));
  kalman_filter_matrix_set_element (kf->Rb2n, 1, 1, -q[3]*q[3]+q[2]*q[2]-q[1]*q[1]+q[0]*q[0]);
  kalman_filter_matrix_set_element (kf->Rb2n, 1, 2, 2*(q[2]*q[3]-q[0]*q[1]));

  kalman_filter_matrix_set_element (kf->Rb2n, 2, 0, 2*(q[1]*q[3]-q[0]*q[2]));
  kalman_filter_matrix_set_element (kf->Rb2n, 2, 1, 2*(q[2]*q[3]+q[0]*q[1]));
  kalman_filter_matrix_set_element (kf->Rb2n, 2, 2, q[3]*q[3]-q[2]*q[2]-q[1]*q[1]+q[0]*q[0]);

}

void
kalman_filter_magneto_measurements_normalization (kalman_filter_t * kf)
{
  /*
  z_mr = R_b^n z_m
  z_mr[2] = 0
  z_mr = z_mr / norm(z_mr)
  z_m = R_n^b z_mr
  */
  double norm;

  kalman_filter_change_rep_to_navigation (kf);

  kalman_filter_matrix_set_zero (kf->z_mr);
  kalman_filter_matrix_mult (kf->Rb2n, kf->z, kf->z_mr);

  kalman_filter_matrix_set_element (kf->z_mr, 2, 0, 0);

  norm = kf->z_mr.mat[0][0]*kf->z_mr.mat[0][0];
  norm +=kf->z_mr.mat[1][0]*kf->z_mr.mat[1][0];
  norm +=kf->z_mr.mat[2][0]*kf->z_mr.mat[2][0];
  norm = sqrt(norm);

  kalman_filter_matrix_scale (kf->z_mr, 1/norm);

  kalman_filter_matrix_set_zero (kf->z);
  kalman_filter_matrix_transpose (kf->Rb2n, kf->Rn2b);
  kalman_filter_matrix_mult (kf->Rn2b, kf->z_mr, kf->z);

}

void
kalman_filter_free (kalman_filter_t *kf)
{
  kalman_filter_matrix_free(kf->x);
  kalman_filter_matrix_free(kf->x_);
  kalman_filter_matrix_free(kf->P);
  kalman_filter_matrix_free(kf->P_);
  kalman_filter_matrix_free(kf->F);
  kalman_filter_matrix_free(kf->FT);
  kalman_filter_matrix_free(kf->Ha);
  kalman_filter_matrix_free(kf->HaT);
  kalman_filter_matrix_free(kf->Hm);
  kalman_filter_matrix_free(kf->HmT);
  kalman_filter_matrix_free(kf->Q);
  kalman_filter_matrix_free(kf->Ra);
  kalman_filter_matrix_free(kf->Rm);
  kalman_filter_matrix_free(kf->FP);
  kalman_filter_matrix_free(kf->z);
  kalman_filter_matrix_free(kf->z_hat);
  kalman_filter_matrix_free(kf->y);
  kalman_filter_matrix_free(kf->xe);
  kalman_filter_matrix_free(kf->S);
  kalman_filter_matrix_free(kf->S_inv);
  kalman_filter_matrix_free(kf->PHT);
  kalman_filter_matrix_free(kf->KH);
  kalman_filter_matrix_free(kf->KHP);
  kalman_filter_matrix_free(kf->PNJ);
  kalman_filter_matrix_free(kf->K);
  kalman_filter_matrix_free(kf->I);
  kalman_filter_matrix_free(kf->Rb2n);
  kalman_filter_matrix_free(kf->Rn2b);
  kalman_filter_matrix_free(kf->z_mr);
  kalman_filter_matrix_free(kf->norm_jacobian);
  kalman_filter_matrix_free(kf->norm_jacobianT);
}

void
kalman_filter_matrix_free (matrix_t m)
{
  free(m.mat[0]);
  free(m.mat);
}
