#!/usr/bin/env python

import sys
import numpy as np
import matplotlib.pyplot as plt

gt_q0, gt_q1, gt_q2, gt_q3 = [], [], [], []
ekf_q0 = []; ekf_q1 = []; ekf_q2 = []; ekf_q3 = []
accel_x, accel_y, accel_z = [], [], []
gyro_x, gyro_y, gyro_z = [], [], []
magneto_x, magneto_y, magneto_z = [], [], []
timestamp = []
timestamp_gt = []
timestamp_imu = []


for line in sys.stdin:
    l = line.split()
    timestamp.append(float(l[0]))
    ekf_q0.append(float(l[1]))
    ekf_q1.append(float(l[2]))
    ekf_q2.append(float(l[3]))
    ekf_q3.append(float(l[4]))

f = open("./gt.datalog", "r")
for line in f.readlines():
    l = line.split()
    timestamp_gt.append(float(l[0]))
    gt_q0.append(float(l[1]))
    gt_q1.append(float(l[2]))
    gt_q2.append(float(l[3]))
    gt_q3.append(float(l[4]))

f.close()

f = open("./imu.datalog", "r")
for line in f.readlines():
    l = line.split()
    timestamp_imu.append(float(l[0]))
    accel_x.append(float(l[1]))
    accel_y.append(float(l[2]))
    accel_z.append(float(l[3]))
    gyro_x.append(float(l[4]))
    gyro_y.append(float(l[5]))
    gyro_z.append(float(l[6]))
    magneto_x.append(float(l[7]))
    magneto_y.append(float(l[8]))
    magneto_z.append(float(l[9]))

f.close()

t_max = timestamp_gt[-1]

fig = plt.figure('Accelerometer measurements')
plt.subplot(311)
plt.plot(timestamp_imu,accel_x, 'b', label='Accelerometer measurements')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)
plt.legend()

plt.subplot(312)
plt.plot(timestamp_imu,accel_y, 'b')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)

plt.subplot(313)
plt.plot(timestamp_imu,accel_z, 'b')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)

fig = plt.figure('Gyroscope measurements')
plt.subplot(311)
plt.plot(timestamp_imu, gyro_x, 'b', label='Gyroscope measurements')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)
plt.legend()

plt.subplot(312)
plt.plot(timestamp_imu, gyro_y, 'b')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)

plt.subplot(313)
plt.plot(timestamp_imu, gyro_z, 'b')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)

fig = plt.figure('Magnetometer measurements')
plt.subplot(311)
plt.plot(timestamp_imu, magneto_x, 'b', label='Magnetometer measurements')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)
plt.legend()

plt.subplot(312)
plt.plot(timestamp_imu, magneto_y, 'b')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)

plt.subplot(313)
plt.plot(timestamp_imu, magneto_z, 'b')
#plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)

fig = plt.figure('Estimated quaternion')
plt.subplot(411)
plt.plot(timestamp, ekf_q0, 'r', label='EKF estimation')
plt.plot(timestamp_gt, gt_q0, '--k', label='GT')
plt.ylim(0.9,1.2)
#plt.xlim(0,t_max)
plt.legend()

plt.subplot(412)
plt.plot(timestamp, ekf_q1, 'r')
plt.plot(timestamp_gt, gt_q1, '--k')
plt.ylim(-0.8,0.4)
#plt.xlim(0,t_max)

plt.subplot(413)
plt.plot(timestamp, ekf_q2, 'r')
plt.plot(timestamp_gt, gt_q2, '--k')
plt.ylim(-0.2,0.4)
#plt.xlim(0,t_max)

plt.subplot(414)
plt.plot(timestamp, ekf_q3, 'r')
plt.plot(timestamp_gt, gt_q3, '--k')
plt.ylim(-0.4,0.0)
#plt.xlim(0,t_max)

plt.show()
