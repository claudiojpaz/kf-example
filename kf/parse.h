#ifndef PARSE_H
#define PARSE_H

#include "kalman.h"

int
count_lines_in_file (char *);

int
parse_sensors_file (char *, sensors_t *);

int
parse_GT_file (char *, sensors_t *);

int
parse_AGM_in_sensors_file (char *, sensors_t *);

void
parse_params_file (char *, kalman_filter_params_t *);

#endif
