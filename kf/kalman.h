#ifndef KALMAN_H
#define KALMAN_H

#include "sensors.h"

typedef struct {
  double **mat;
  int row;
  int col;
} matrix_t;

typedef struct
{
  double p11;        // Diagonal elements of covariance of the state
  double p22;
  double p33;
  double p44;
  double q11;        // Diagonal elements of covariance of the process noise
  double q22;
  double q33;
  double q44;
  double raxx;        // Diagonal elements of covariance of the accelerometer observation noise
  double rayy;
  double razz;
  double rmxx;        // Diagonal elements of covariance of the magnetometer observation noise
  double rmyy;
  double rmzz;
} kalman_filter_params_t;

typedef struct
{
  matrix_t x;             // State
  matrix_t x_;            // State (prior)
  matrix_t P;             // Covariance of the state
  matrix_t P_;            // Covariance of the state (prior)
  matrix_t F;             // Transition model
  matrix_t FT;            // F^T
  matrix_t Ha;            // Accelerometer observation model
  matrix_t HaT;           // Ha^T
  matrix_t Hm;            // Magnetometer observation model
  matrix_t HmT;           // Hm^T
  matrix_t Q;             // Covariance of the process noise
  matrix_t Ra;            // Covariance of the accelerometer observation noise
  matrix_t Rm;            // Covariance of the magnetometer observation noise
  matrix_t FP;            // intermediate product matrix
  matrix_t z;             // Measurement
  matrix_t z_hat;         // Measurement prediction
  matrix_t y;             // Innovation
  matrix_t xe;            // State error
  matrix_t S;
  matrix_t S_inv;
  matrix_t PHT;           // intermediate product matrix
  matrix_t KH;            // intermediate product matrix
  matrix_t KHP;           // intermediate product matrix
  matrix_t PNJ;           // intermediate product matrix
  matrix_t K;             // Kalman gain matrix
  matrix_t I;             // Just an Identity
  matrix_t norm_jacobian;
  matrix_t norm_jacobianT;
  matrix_t Rb2n;          // Change reference system matrix
  matrix_t Rn2b;          // Change reference system matrix
  matrix_t z_mr;            // temporary vector
} kalman_filter_t;

void
kalman_filter_init (kalman_filter_t *, kalman_filter_params_t *);

void
kalman_filter_free (kalman_filter_t *);

void
kalman_filter_alloc (kalman_filter_t *);

void
kalman_filter_set_Fk (kalman_filter_t *, sensor3D_t *, float);

void
kalman_filter_set_Hak (kalman_filter_t *, double *);

void
kalman_filter_set_Hmk (kalman_filter_t *, double *);

void
kalman_filter_set_z_hat_ak (kalman_filter_t *);

void
kalman_filter_set_z_hat_mk (kalman_filter_t *);

void
kalman_filter_predict_stage (kalman_filter_t *, sensor3D_t *, float);

void
kalman_filter_accel_update_stage (kalman_filter_t *, sensor3D_t *);

void
kalman_filter_mag_update_stage (kalman_filter_t *, sensor3D_t *);

void
kalman_filter_quaternion_normalization (kalman_filter_t *);

void
kalman_filter_change_rep_to_navigation (kalman_filter_t *);

void
kalman_filter_magneto_measurements_normalization (kalman_filter_t *);

void
kalman_filter_matrix_set_identity (matrix_t, int);

void
kalman_filter_matrix_set_zero (matrix_t);

void
kalman_filter_matrix_set_element (matrix_t, int, int, double);

void
kalman_filter_matrix_scale (matrix_t, double);

void
kalman_filter_matrix_add (matrix_t, matrix_t);

void
kalman_filter_matrix_sub (matrix_t, matrix_t);

void
kalman_filter_matrix_mult (matrix_t, matrix_t, matrix_t);

void
kalman_filter_matrix_transpose (matrix_t, matrix_t);

void
kalman_filter_matrix_memcpy (matrix_t, matrix_t);

double
kalman_filter_matrix_det (matrix_t);

void
kalman_filter_matrix_scale_adjoint (matrix_t, matrix_t, double);

void
kalman_filter_matrix_invert (matrix_t, matrix_t);

double
kalman_filter_matrix_get_element (matrix_t, int, int);

matrix_t
kalman_filter_matrix_alloc (int, int);

void
kalman_filter_matrix_free (matrix_t);


#endif
