#ifndef SENSORS_H
#define SENSORS_H

typedef struct
{
  float x;
  float y;
  float z;
} sensor3D_t;

typedef struct
{
  int n;
  float *timestamp;
  sensor3D_t *accel;
  sensor3D_t *gyro;
  sensor3D_t *magneto;
} sensors_t;

int
sensors_array_alloc ( sensors_t *, int);

sensor3D_t *
sensor3D_array_alloc (int);

void
sensors_array_free (sensors_t * );

int
sensors_array_print (sensors_t * );

#endif
