#include <stdio.h>
#include <stdlib.h>

#include "parse.h"
#include "kalman.h"
#include "sensors.h"

int
count_lines_in_file (char *input_file)
{
  FILE *p_file;
  char c;
  int n_lines = 0;

  p_file = fopen (input_file, "r");
  if (p_file == NULL)
  {
    fprintf (stderr, "Cannot open file %s\n", input_file);
    exit (EXIT_FAILURE);
  }

  while ((c = fgetc (p_file)) != EOF)
    if (c == '\n')
      n_lines++;

  fclose (p_file);

  return n_lines;
}

int
parse_sensors_file (char *input_file, sensors_t * s)
{
  int n_lines;

  n_lines = count_lines_in_file (input_file);
  sensors_array_alloc (s, n_lines);
  parse_AGM_in_sensors_file (input_file, s);

  return n_lines;
}

void
parse_params_file (char *input_file, kalman_filter_params_t * p)
{
  FILE *p_file;

  p_file = fopen (input_file, "r");

  fscanf (p_file, "%lf %lf %lf %lf ", &p->p11, &p->p22, &p->p33, &p->p44);
  fscanf (p_file, "%lf %lf %lf %lf ", &p->q11, &p->q22, &p->q33, &p->q44);
  fscanf (p_file, "%lf %lf %lf ", &p->raxx, &p->rayy, &p->razz);
  fscanf (p_file, "%lf %lf %lf ", &p->rmxx, &p->rmyy, &p->rmzz);

  fclose (p_file);
}

int
parse_AGM_in_sensors_file (char *input_file, sensors_t * s)
{
  FILE *p_file;
  int i = 0;

  p_file = fopen (input_file, "r");

  for (i = 0; !feof (p_file); i++ )
  {
    fscanf (p_file, "%f ", &s->timestamp[i]);
    fscanf (p_file, "%f %f %f ", &s->accel[i].x, &s->accel[i].y, &s->accel[i].z);
    fscanf (p_file, "%f %f %f ", &s->gyro[i].x, &s->gyro[i].y, &s->gyro[i].z);
    fscanf (p_file, "%f %f %f ", &s->magneto[i].x, &s->magneto[i].y, &s->magneto[i].z);
  }

  fclose (p_file);

  return i;
}
