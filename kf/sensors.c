#include <stdio.h>
#include <stdlib.h>

#include "sensors.h"

sensor3D_t *
sensor3D_array_alloc (int n)
{
  sensor3D_t * sensor;

  sensor = (sensor3D_t *) malloc ( n * sizeof (sensor3D_t));
  if (sensor == NULL) {
    fprintf (stderr, "Unable to allocate memory\n");
    exit (EXIT_FAILURE);
  }

  return sensor;
}

int
sensors_array_alloc ( sensors_t * sensors, int n)
{
  sensors->timestamp = (float*) malloc ( n * sizeof (float));
  if (sensors->timestamp == NULL) {
    fprintf (stderr, "Unable to allocate memory\n");
    exit (EXIT_FAILURE);
  }

  sensors->accel = sensor3D_array_alloc(n);
  sensors->gyro = sensor3D_array_alloc(n);
  sensors->magneto = sensor3D_array_alloc(n);

  sensors->n = n;

  return n;
}

void
sensors_array_free (sensors_t * s)
{
  if (s->timestamp) free (s->timestamp);
  if (s->accel) free (s->accel);
  if (s->gyro) free (s->gyro);
  if (s->magneto) free (s->magneto);
}

int
sensors_array_print (sensors_t * s)
{
  int i;

  for ( i = 0; i < s->n; i++ )
  {
    printf ("%f ", s->timestamp[i]);
    printf ("%f %f %f ", s->accel[i].x, s->accel[i].y, s->accel[i].z);
    printf ("%f %f %f ", s->gyro[i].x, s->gyro[i].y, s->gyro[i].z);
    printf ("%f %f %f \n", s->magneto[i].x, s->magneto[i].y, s->magneto[i].z);
  }

  return i;
}
