Kalman Filter for quaternion estimation using GLS
=================================================

This is an example of Kalman Filter for quaternion estimation using GNU Scientific Library.

Requisite
----------

* Docker

Use
---

Clone this with
```bash
git clone git@gitlab.com:claudiojpaz/kf-example.git
```

After that,
```bash
cd kf-example
./build-debian-dev-gsl
./start-debian-dev-gsl
```
Once inside the container, go to `kf` folder

```bash
cd kf
make
./kf -i imu.datalog -p kf_parameters.param
```

The output of the algorithm is timestamp and four scalar values belonging to estimated quaternion.
Also, data can be ploted using a python script

```bash
./kf -i imu.datalog -p kf_parameters.param | python3 stdin_plot.py
```
